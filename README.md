Book Timeline
=============

Book Timeline is a Javascript project that displays the books that I've read, shows the author of each book, gives a brief description, while also showing the book I'm currently reading. 

Author pages, wishlists, and descriptions of books are provided by Amazon. 

The website is built on the <a href="https://github.com/technotarek/timeliner">`timeliner.js`</a> Javascript library.

See this website in action: <a href="http://avijeets.com/books/">http://avijeets.com/books/</a>

Built your own timeline? <a href="mailto:a@avijeets.com?Subject=I made my own timeline!">Drop me a note!</a>
